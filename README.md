## Onlyoffice document server

ONLYOFFICE Document Server is an online office suite comprising viewers and editors for texts, spreadsheets and presentations, fully compatible with
Office Open XML formats: .docx, .xlsx, .pptx and enabling collaborative editing in real time.

## Bootstrap

```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/onlyoffice-documentserver
```

## Usage

```shell
bastille template TARGET bastillebsd-templates/onlyoffice-documentserver --arg JAIL_IP="jail ip"
```

### Available arguments `--arg NAME=value`
- JAIL_IP=localhost
- DB_NAME=onlyoffice
- DB_USERNAME=onlyoffice
- DB_PASSWORD=onlyoffice
- RABBITMQ_USERNAME=onlyoffice
- RABBITMQ_PASSWORD=password
